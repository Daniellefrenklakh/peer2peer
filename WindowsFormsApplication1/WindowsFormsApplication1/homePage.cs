﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net.Http;
using System.Diagnostics;
using System.Net;
using System.Threading;

namespace WindowsFormsApplication1
{
    /*public class Win32
    {
        [DllImport("User32.Dll")]
        public static extern long SetCursorPos(int x, int y);

        [DllImport("User32.Dll")]
        public static extern bool ClientToScreen(IntPtr hWnd, ref POINT point);

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int x;
            public int y;
        }
    } */
    public partial class homePage : Form
    {
        //[DllImport(@".\simplest_ffmpeg_receiver.dll")]
        // [DllImport(@".\Connection2.dll")]

        static string id;
        static string psw;

        public homePage()
        {
            InitializeComponent();
            myID.Text = id;
            myPsw.Text = psw;     
        }
        public homePage(string new_id, string new_psw)
        {
            id = new_id;
            psw = new_psw;

            InitializeComponent();
            myID.Text = id;
            myPsw.Text = psw;            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //while not asked to connect by another user
            allowConnectBtm.Enabled = false;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

       /*private void Change_Click(object sender, EventArgs e)
        {
            string btPw = "";
            btPw = CreatePassword(8);
            myPsw.Text = btPw;
        }*/

        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        /*private void Set_Click(object sender, EventArgs e)
        {
            setPswd m = new setPswd(this);
            m.Show();
            this.Hide();
        }*/

        private void MyPassword_Click(object sender, EventArgs e)
        {

        }
        private void form1_KeyPress(object sender, KeyPressEventArgs e)
        {

            Console.WriteLine(e.KeyChar);
        }
        private void connectBtm_Click(object sender, EventArgs e)
        {
            //if connection allowed
           
            
            //string PartnerId = PartnerID.Text;
            //string PartnersPsw = PartnerPsw.Text;

            Dictionary<string, string> data = new Dictionary<string, string>()
            { 
                {"username", PartnerID.Text },
                { "password", PartnerPsw.Text}
            };
            HttpHandler handler = new HttpHandler("http://localHost:3000/connect/");
            string received_ip = handler.Post(data).Result;
            Console.WriteLine(received_ip);

            //string strCmdText;
            //strCmdText = "ffplay -f mpegts udp://127.0.0.1:7654";//client
            //strCmdText = "ffmpeg -f gdigrab -i desktop -f mpegts udp:127.0.0.1:7654";//server
            //system.Diagnostics.Process.Start("CMD.exe", strCmdText);

            var thread_cmd_handler = new Thread(cmd_handler);
            cmd_handler(received_ip);
            // ThreadStart cmdThread = new ThreadStart(cmd_handler);
            this.Hide();
        }

        private void cmd_handler()
        { }


        private void cmd_handler(string received_ip)
        {
            Process cmd = new Process();
            cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            //string command = "ffplay -f mpegts udp://";
            string command = "ffmpeg - f gdigrab - i desktop - f mpegts udp:";
            string ip = received_ip;
            string port = ":5643";
            cmd.StandardInput.WriteLine(command + ip + port);
            //cmd.StandardInput.WriteLine("ffmpeg -f gdigrab -i desktop -f mpegts udp:127.0.0.1:7654");

            //cmd.StandardInput.Flush();
            //cmd.StandardInput.Close();
            //cmd.WaitForExit();
            Console.WriteLine(cmd.StandardOutput.ReadToEnd());


            //Socket s = new Socket(AddressFamily.InterNetwork,SocketType.Stream, ProtocolType.Tcp);  
            //viewScreen m = new viewScreen();
            //m.Show();
        }
        private void open_command_line()
        {
           

        }
        private void PartnerID_TextChanged(object sender, EventArgs e)
        {

        }

        private void allowConnectBtm_Click(object sender, EventArgs e)
        {

            Process cmd = new Process();
            cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();

            
            cmd.StandardInput.WriteLine("ffmpeg -f gdigrab -i desktop -f mpegts udp:127.0.0.1:7654");

            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
            Console.WriteLine(cmd.StandardOutput.ReadToEnd());
        }

        private void back_Click(object sender, EventArgs e)
        {
            this.Hide();
            register rg = new register();
            rg.Show();

        }

        
    }
}
