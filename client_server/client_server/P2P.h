#ifndef P2P_H
#define P2P_H
#pragma comment(lib, "Ws2_32.lib")
#include <iomanip>

#include <stdio.h>
#include <iostream>
#include <Windows.h>
#include <winsock.h>
#include <stdexcept>
#include <vector>
#include <cstring>
#include <memory>
#include <map>

#include "gdiplus.h"
using namespace Gdiplus;
using namespace Gdiplus::DllExports;
#define PORT 3344

void saveBmp(HBITMAP bmp, std::string filename);

class P2P{
public:
	P2P();
	~P2P();
	virtual int connectTo() = 0;
	//void bmp_recv(HBITMAP bmp);
	void bmp_send(HBITMAP bmp);
protected:
	SOCKET _ListenSocket;
	SOCKET _server;
};


#endif